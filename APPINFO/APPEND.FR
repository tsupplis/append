Begin3
Language:    FR, 850, French (Standard)
Title:       APPEND
Description: Acc�der aux fichiers comme si dans le dossier courant
Summary:     APPEND permet aux programmes d'ouvrir les fichiers de donn�es dans les r�pertoires sp�cifi�s comme si les fichiers se trouvaient dans le r�pertoire courant.
Keywords:    freedos, joindre, donn�es, fichiers, r�pertoires
End
