# APPEND

APPEND enables programs to open data files in specified directories as if the files were in the current directory.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## APPEND.LSM

<table>
<tr><td>title</td><td>APPEND</td></tr>
<tr><td>version</td><td>5.0-0.6a</td></tr>
<tr><td>entered&nbsp;date</td><td>2006-01-23</td></tr>
<tr><td>description</td><td>Access files as if in the current dir</td></tr>
<tr><td>summary</td><td>APPEND enables programs to open data files in specified directories as if the files were in the current directory.</td></tr>
<tr><td>keywords</td><td>freedos, append, data, files, directories</td></tr>
<tr><td>author</td><td>Eduardo Casino-Almao &lt;mail@eduardocasino.es&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eduardo Casino-Almao &lt;mail@eduardocasino.es&gt;</td></tr>
<tr><td>platforms</td><td>DOS (nasm)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Append</td></tr>
</table>
